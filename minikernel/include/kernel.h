/*
 *  minikernel/include/kernel.h
 *
 *  Minikernel. Versi�n 1.0
 *
 *  Fernando P�rez Costoya
 *
 */

/*
 *
 * Fichero de cabecera que contiene definiciones usadas por kernel.c
 *
 *      SE DEBE MODIFICAR PARA INCLUIR NUEVA FUNCIONALIDAD
 *
 */

#ifndef _KERNEL_H
#define _KERNEL_H

#include "const.h"
#include "HAL.h"
#include "llamsis.h"

#define NO_REC 0
#define REC 1


//Definicion de la struct de los mutex

typedef struct{
   char *nombre;
   int bloqueos;
   int proc_bloq;
   int id_procesos[MAX_PROC];
   int id_procesos_bloqueados[MAX_PROC];
   int tipo;
}Mutex;




/*
 *
 * Definicion del tipo que corresponde con el BCP.
 * Se va a modificar al incluir la funcionalidad pedida.
 *
 */
typedef struct BCP_t *BCPptr;

typedef struct BCP_t {
        int id;				/* ident. del proceso */
        int estado;			/* TERMINADO|LISTO|EJECUCION|BLOQUEADO*/
        contexto_t contexto_regs;	/* copia de regs. de UCP */
        void * pila;			/* dir. inicial de la pila */
	BCPptr siguiente;		/* puntero a otro BCP */
	void *info_mem;			/* descriptor del mapa de memoria */
	int t_inicio_bloqueo;   /*Momento en el que se inicia el bloqueo*/
	int t_segundos_bloqueo; /*Tiempo que va a bloquearse el proceso. Coincide con el par�metro "segudos" de la funci�n dormir*/
	int tics_a_dormir;
	int tics_rodaja;
	int t_sistema;
	int t_usuario;
    int nMutAsociados;
	Mutex *mutex_proceso[NUM_MUT_PROC];
} BCP;

/*
 *
 * Definicion del tipo que corresponde con la cabecera de una lista
 * de BCPs. Este tipo se puede usar para diversas listas (procesos listos,
 * procesos bloqueados en sem�foro, etc.).
 *
 */

typedef struct{
	BCP *primero;
	BCP *ultimo;
} lista_BCPs;


/*
 * Variable global que identifica el proceso actual
 */

BCP * p_proc_actual=NULL;

/*
 * Variable global que representa la tabla de procesos
 */

BCP tabla_procs[MAX_PROC];

/*
 * Variable global que representa la cola de procesos listos
 */
lista_BCPs lista_listos= {NULL, NULL};
lista_BCPs lista_bloqueados = {NULL,NULL};
lista_BCPs lista_bloqueados_mut = {NULL,NULL};
lista_BCPs lista_bloq_lock = {NULL,NULL};
lista_BCPs lista_bloq_caracteres = {NULL,NULL};
/*
 *
 * Definici�n del tipo que corresponde con una entrada en la tabla de
 * llamadas al sistema.
 *
 */
typedef struct{
	int (*fservicio)();
} servicio;

//Definicion de struct tiempos_ejec
struct tiempos_ejec{
    int usuario;
	int sistema;
};

Mutex mutex_en_sist[NUM_MUT];

//Definicion de buffer de recepcion de caracteres
char caracteres[TAM_BUF_TERM];
int pcharL = 0;
int pcharE = 0;
int contChar = 0;
/*Definici�n de variables globales*/
int contador_actual = 0;//Tics globales
int numMutex = 0; //Numero de mutex en el sistema
int tocandoParametros = 0;
/*
 * Prototipos de las rutinas que realizan cada llamada al sistema
 */
int sis_crear_proceso();
int sis_terminar_proceso();
int sis_escribir();

int obtener_id_pr();
int tiempos_proceso();
int crear_mutex();
int abrir_mutex();
int cerrar_mutex();
int lock();
int unlock();
int cerrar_mutex();
int leer_caracter();
int dormir();




/*
 * Variable global que contiene las rutinas que realizan cada llamada
 */
servicio tabla_servicios[NSERVICIOS]={	
					{sis_crear_proceso},
					{sis_terminar_proceso},
					{sis_escribir},
					{obtener_id_pr},
					{dormir},
					{tiempos_proceso}, 
					{crear_mutex}, 
					{abrir_mutex}, 
					{lock}, 
					{unlock}, 
					{cerrar_mutex},
					{leer_caracter}};

#endif /* _KERNEL_H */

