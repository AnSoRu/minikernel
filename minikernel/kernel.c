/*
 *  kernel/kernel.c
 *
 *  Minikernel. Versi�n 1.0
 *
 *  Fernando P�rez Costoya
 *
 */

/*
 *
 * Fichero que contiene la funcionalidad del sistema operativo
 *
 */

#include "kernel.h"	/* Contiene defs. usadas por este modulo */
#include <unistd.h>
#include <string.h>
/*
 *
 * Funciones relacionadas con la tabla de procesos:
 *	iniciar_tabla_proc buscar_BCP_libre
 *
 */

/*
 * Funci�n que inicia la tabla de procesos
 */
static void iniciar_tabla_proc(){
	int i;

	for (i=0; i<MAX_PROC; i++)
		tabla_procs[i].estado=NO_USADA;
}

/*
 * Funci�n que busca una entrada libre en la tabla de procesos
 */
static int buscar_BCP_libre(){
	int i;

	for (i=0; i<MAX_PROC; i++)
		if (tabla_procs[i].estado==NO_USADA)
			return i;
	return -1;
}

/*
 *
 * Funciones que facilitan el manejo de las listas de BCPs
 *	insertar_ultimo eliminar_primero eliminar_elem
 *
 * NOTA: PRIMERO SE DEBE LLAMAR A eliminar Y LUEGO A insertar
 */

/*
 * Inserta un BCP al final de la lista.
 */
static void insertar_ultimo(lista_BCPs *lista, BCP * proc){
	if (lista->primero==NULL)
		lista->primero= proc;
	else
		lista->ultimo->siguiente=proc;
	lista->ultimo= proc;
	proc->siguiente=NULL;
}

/*
 * Elimina el primer BCP de la lista.
 */
static void eliminar_primero(lista_BCPs *lista){

	if (lista->ultimo==lista->primero)
		lista->ultimo=NULL;
	lista->primero=lista->primero->siguiente;
}

/*
 * Elimina un determinado BCP de la lista.
 */
static void eliminar_elem(lista_BCPs *lista, BCP * proc){
	BCP *paux=lista->primero;

	if (paux==proc)
		eliminar_primero(lista);
	else {
		for ( ; ((paux) && (paux->siguiente!=proc));
			paux=paux->siguiente);
		if (paux) {
			if (lista->ultimo==paux->siguiente)
				lista->ultimo=paux;
			paux->siguiente=paux->siguiente->siguiente;
		}
	}
}

static void imprimir_lista(lista_BCPs *lista){
   BCP *proc_print=lista->primero;
   if(proc_print!=NULL){
	 printk("ID:%d, Estado Primero:%d\n",proc_print->id,proc_print->estado);
   }
   //while(proc_print!=lista->ultimo){
	 //  proc_print = proc_print->siguiente;
	//printk("ID:%d, Estado:%d\n",proc_print->id,proc_print->estado);
   //}
   //printk("ID:%d, Estado Ultimo:%d\n",proc_print->id,proc_print->estado);
}

/*
 *
 * Funciones relacionadas con la planificacion
 *	espera_int planificador
 */

/*
 * Espera a que se produzca una interrupcion
 */
static void espera_int(){
	int nivel;

	printk("-> NO HAY LISTOS. ESPERA INT\n");

	/* Baja al m�nimo el nivel de interrupci�n mientras espera */
	nivel=fijar_nivel_int(NIVEL_1);
	halt();
	fijar_nivel_int(nivel);
}

/*
 * Funci�n de planificacion que implementa un algoritmo FIFO.
 */
static BCP * planificador(){

	while (lista_listos.primero==NULL)
		espera_int();		/* No hay nada que hacer */
	lista_listos.primero->tics_rodaja=TICKS_POR_RODAJA;
	return lista_listos.primero;
}

/*
 *
 * Funcion auxiliar que termina proceso actual liberando sus recursos.
 * Usada por llamada terminar_proceso y por rutinas que tratan excepciones
 *
 */
static void liberar_proceso(){
	BCP * p_proc_anterior;
	//Liberacion de los mutex
	int j;
	int aux;

	for(j=0;j<NUM_MUT_PROC;j++){
	   if(p_proc_actual->mutex_proceso[j]!=NULL){
		escribir_registro(1,j);
		aux = cerrar_mutex(j);
	   }
	}
	liberar_imagen(p_proc_actual->info_mem); /* liberar mapa */

	p_proc_actual->estado=TERMINADO;
	int nivel_interrupcion = fijar_nivel_int(NIVEL_3);
	eliminar_primero(&lista_listos); /* proc. fuera de listos */
	fijar_nivel_int(nivel_interrupcion);

	/* Realizar cambio de contexto */
	p_proc_anterior=p_proc_actual;
	p_proc_actual=planificador();

	printk("-> C.CONTEXTO POR FIN: de %d a %d\n",
			p_proc_anterior->id, p_proc_actual->id);

	liberar_pila(p_proc_anterior->pila);
	cambio_contexto(NULL, &(p_proc_actual->contexto_regs));
        return; /* no deber�a llegar aqui */
}

/*
 *
 * Funciones relacionadas con el tratamiento de interrupciones
 *	excepciones: exc_arit exc_mem
 *	interrupciones de reloj: int_reloj
 *	interrupciones del terminal: int_terminal
 *	llamadas al sistemas: llam_sis
 *	interrupciones SW: int_sw
 *
 */

/*
 * Tratamiento de excepciones aritmeticas
 */
static void exc_arit(){

	if (!viene_de_modo_usuario())
		panico("excepcion aritmetica cuando estaba dentro del kernel");


	printk("-> EXCEPCION ARITMETICA EN PROC %d\n", p_proc_actual->id);
	liberar_proceso();

        return; /* no deber�a llegar aqui */
}

/*
 * Tratamiento de excepciones en el acceso a memoria
 */
static void exc_mem(){

	if (!viene_de_modo_usuario()&& tocandoParametros!=1)
		panico("excepcion de memoria cuando estaba dentro del kernel");


	printk("-> EXCEPCION DE MEMORIA EN PROC %d\n", p_proc_actual->id);
	liberar_proceso();

        return; /* no deber�a llegar aqui */
}

/*
 * Tratamiento de interrupciones de terminal
 * La interrupcion de terminal PRODUCE caracteres
 */
static void int_terminal(){
	char car;
	car = leer_puerto(DIR_TERMINAL);
	printk("-> TRATANDO INT. DE TERMINAL %c\n", car);
	int nivel_interrupcion;
	//Comprobar si hay espacio disponible para albergar al nuevo caracter
	if(contChar<TAM_BUF_TERM){
		if(pcharE == TAM_BUF_TERM){
			pcharE = 0;
		}
		//nivel_interrupcion = fijar_nivel_int(NIVEL_1);
		caracteres[pcharE] = car;
		pcharE++;
		contChar++;
		//fijar_nivel_int(nivel_interrupcion);
		//Ahora falta desbloquear a los procsos que esten esperando un caracter
		BCP *bloqueado = lista_bloq_caracteres.primero;
		if(bloqueado!=NULL){
             bloqueado->estado = LISTO;
			 nivel_interrupcion = fijar_nivel_int(NIVEL_3);
			 eliminar_elem(&lista_bloq_caracteres,bloqueado);
			 insertar_ultimo(&lista_listos,bloqueado);
			 fijar_nivel_int(nivel_interrupcion);
		}
	}
  return;
}

/*
 * Tratamiento de interrupciones de reloj
 */
static void int_reloj(){
    BCP *proceso = lista_listos.primero;
	int nivel_interrupcion;
	if(proceso!=NULL){
	   p_proc_actual->tics_rodaja--;
	   printk("TICS RODAJA %d\n",p_proc_actual->tics_rodaja);
	   if(p_proc_actual->tics_rodaja == 0){
            activar_int_SW();
			//return;
	   }

	 if(viene_de_modo_usuario()){
	   p_proc_actual->t_usuario++;
	   /////////////////////////////////////
	   /////////////////////////////////////
	 }
	 else{
	   p_proc_actual->t_sistema++;
	   /////////////////////////////////////
	   /*
	   p_proc_actual->tics_rodaja--;
	   printk("TICS RODAJA %d\n",p_proc_actual->tics_rodaja);
	   if(p_proc_actual->tics_rodaja == 0){
		  activar_int_SW();
		  //return;
	   }
	   */
	   ///////////////////////////////////7/
	 }
	}
	nivel_interrupcion = fijar_nivel_int(NIVEL_3);
	contador_actual++;
	fijar_nivel_int(nivel_interrupcion);
	printk("-> TRATANDO INT. DE RELOJ\n");
	printk("En modo usuario %d\n",p_proc_actual->t_usuario);
	printk("En modo sistema %d\n",p_proc_actual->t_sistema);
	printk("Contador actual %d\n",contador_actual);
	BCP *bloqueado=lista_bloqueados.primero;
	BCP *siguiente;
	//int nivel_interrupcion;
	while(bloqueado!=NULL){
	  siguiente=bloqueado->siguiente;
	  if((bloqueado->tics_a_dormir)>0){
		bloqueado->tics_a_dormir = bloqueado->tics_a_dormir - 1;
	  }
	  //if((contador_actual-bloqueado->t_inicio_bloqueo) >= (TICK*bloqueado->t_segundos_bloqueo)){
	  if(bloqueado->tics_a_dormir == 0){
	    bloqueado->estado = LISTO;
		nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		eliminar_elem(&lista_bloqueados,bloqueado);
		insertar_ultimo(&lista_listos,bloqueado);
		fijar_nivel_int(nivel_interrupcion);
	  }
	  bloqueado=siguiente;
	}
   return;
}

/*
 * Tratamiento de llamadas al sistema
 */
static void tratar_llamsis(){
	int nserv, res;

	nserv=leer_registro(0);
	if (nserv<NSERVICIOS)
		res=(tabla_servicios[nserv].fservicio)();
	else
		res=-1;		/* servicio no existente */
	escribir_registro(0,res);
	return;
}

/*
 * Tratamiento de interrupciuones software
 */
static void int_sw(){

	//Realizar cambio de contexto del proceso actual pasandolo al final de la
	//lista de listos
	printk("-> TRATANDO INT.SW\n");
	int nivel_interrupcion = fijar_nivel_int(NIVEL_3);
	BCP *proceso = p_proc_actual;
	if (p_proc_actual->tics_rodaja==0) {
	eliminar_elem(&lista_listos,proceso);
	insertar_ultimo(&lista_listos,proceso);
	fijar_nivel_int(nivel_interrupcion);

	 p_proc_actual = planificador();
	p_proc_actual->tics_rodaja=TICKS_POR_RODAJA;
	cambio_contexto(&(proceso->contexto_regs),&(p_proc_actual->contexto_regs));
	}

	//printk("-> TRATANDO INT. SW\n");

	return;
}

/*
 *
 * Funcion auxiliar que crea un proceso reservando sus recursos.
 * Usada por llamada crear_proceso.
 *
 */
static int crear_tarea(char *prog){
	void * imagen, *pc_inicial;
	int error=0;
	int proc;
	BCP *p_proc;

	proc=buscar_BCP_libre();
	if (proc==-1)
		return -1;	/* no hay entrada libre */

	/* A rellenar el BCP ... */
	p_proc=&(tabla_procs[proc]);

	/* crea la imagen de memoria leyendo ejecutable */
	imagen=crear_imagen(prog, &pc_inicial);
	if (imagen)
	{
		p_proc->info_mem=imagen;
		p_proc->pila=crear_pila(TAM_PILA);
		fijar_contexto_ini(p_proc->info_mem, p_proc->pila, TAM_PILA,
			pc_inicial,
			&(p_proc->contexto_regs));
		p_proc->id=proc;
		p_proc->estado=LISTO;
        p_proc->t_inicio_bloqueo=0;
		p_proc->t_segundos_bloqueo=0;
		p_proc->tics_a_dormir=0;
		p_proc->tics_rodaja=TICKS_POR_RODAJA;
		p_proc->t_sistema=0;
		p_proc->t_usuario=0;
		p_proc->nMutAsociados=0;
		int nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		/* lo inserta al final de cola de listos */
		insertar_ultimo(&lista_listos, p_proc);
		fijar_nivel_int(nivel_interrupcion);
		error= 0;
	}
	else
		error= -1; /* fallo al crear imagen */

	return error;
}

/*
 *
 * Rutinas que llevan a cabo las llamadas al sistema
 *	sis_crear_proceso sis_escribir
 *
 */

/*
 * Tratamiento de llamada al sistema crear_proceso. Llama a la
 * funcion auxiliar crear_tarea sis_terminar_proceso
 */
int sis_crear_proceso(){
	char *prog;
	int res;

	printk("-> PROC %d: CREAR PROCESO\n", p_proc_actual->id);
	prog=(char *)leer_registro(1);
	res=crear_tarea(prog);
	return res;
}

/*
 * Tratamiento de llamada al sistema escribir. Llama simplemente a la
 * funcion de apoyo escribir_ker
 */
int sis_escribir()
{
	char *texto;
	unsigned int longi;

	texto=(char *)leer_registro(1);
	longi=(unsigned int)leer_registro(2);

	escribir_ker(texto, longi);
	return 0;
}

/*
 * Tratamiento de llamada al sistema terminar_proceso. Llama a la
 * funcion auxiliar liberar_proceso
 */
int sis_terminar_proceso(){

	printk("-> FIN PROCESO %d\n", p_proc_actual->id);

	liberar_proceso();

        return 0; /* no deber�a llegar aqui */
}

//A�adido el dia 02/03/2017

int obtener_id_pr(){
   return p_proc_actual->id;
}

/*Devuelve el n�mero de interrupciones de reloj que se han producido desde
que arranc� el sistema*/
int tiempos_proceso(){
	/*Si recibo un puntero que no sea nulo hay que apuntar cuantas veces 
	EN LA INTERRUPCI�N DE RELOJ SE HA DETECTADO que el proceso se estaba ejecutando en modo
	usuario o en modo sistema*/
	struct tiempos_ejec *t_ejec;
	t_ejec = (struct tiempos_ejec *)leer_registro(1);
	if (t_ejec != NULL){
		int nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		tocandoParametros = 1;
		fijar_nivel_int(nivel_interrupcion);
		t_ejec->sistema = p_proc_actual->t_sistema;
		t_ejec->usuario = p_proc_actual->t_usuario;
	}
	return contador_actual;
}

int crear_mutex(){

	char *nombre;
	int tipo;

	nombre = (char *)leer_registro(1);
	tipo = (int)leer_registro(2);

	//Comprobar que el nombre es menor que el maximo
	if(strlen(nombre)>MAX_NOM_MUT){
		return -1;
	}
	//Comprobar si quedan descriptores libres al proceso
	if(p_proc_actual->nMutAsociados == NUM_MUT_PROC){
		return -2;
	}
	//Comprobar si hay algun mutex con el mismo nombre
	int i = 0;
	while(i < NUM_MUT ){
	  if(mutex_en_sist[i].nombre != NULL){
		  if((strcmp(mutex_en_sist[i].nombre,nombre) == 0)){
			return -3;
		}
	  }
	  i++;
	}
    printk("En crear mutex\n");
	int nivel_interrupcion;
	//Comprobar si ya se han alcanzado el numero maximo de mutex en el sistema
	//SI -> bloquear proceso hasta liberacion de alguno
	while(numMutex == NUM_MUT){
	 //Caso en el que no hayan mutex en el sistema -> tiene que esperar a que se
	 //libere alguno de ellos
     p_proc_actual->estado = BLOQUEADO;
	 //Hay que insertarlo en la lista de bloquearlos esperando a un mutex
	 nivel_interrupcion = fijar_nivel_int(NIVEL_3);
     eliminar_elem(&lista_listos,p_proc_actual);
	 insertar_ultimo(&lista_bloqueados_mut,p_proc_actual);
	 fijar_nivel_int(nivel_interrupcion);
	 //Como el proceso lo he bloqueado tengo que planificar uno nuevo
	 //Realizo un cambio de contexto
	 BCP *bloqueado = p_proc_actual;
	 p_proc_actual = planificador();
	 cambio_contexto(&(bloqueado->contexto_regs),&(p_proc_actual->contexto_regs));
	 //Ahora ha pasado a ejecutar un proceso que estaba bloqueado
	 //Tenemos que volver a evaluar las condiciones (ver si hay algun mutex
	 //libre con el mismo nombre)
	 i = 0;
	 while (i < NUM_MUT){
		if(mutex_en_sist[i].nombre != NULL){
			if((strcmp(mutex_en_sist[i].nombre,nombre) == 0)){
				return -3;
			}
		}
		i++;
	 }
	}//Fin bucle while
	 int pos;
	 for(i = 0; i < NUM_MUT; i++){
		if(mutex_en_sist[i].nombre == NULL){
			pos = i;
			break;
		}
	 }
	 //nivel_interrupcion = fijar_nivel_int(NIVEL_3);
	 Mutex *miMutex = &(mutex_en_sist[pos]);
	 miMutex->nombre = strdup(nombre);
	 miMutex->tipo = tipo;
	 miMutex->bloqueos = 0;//Es un contador de veces que ha sido bloqueado el mutex
	 miMutex->proc_bloq = 200;  //Utilizo un numero alto para asegurar que ningun proceso tenga realmente este identificador de proceso. Indica el id del proceso que lo ha bloqueado
     for(i = 0; i<MAX_PROC; i++){
		miMutex->id_procesos[i] = 0; //Dos valores 0 no esta bloqueado por el proceso i, 1 esta bloqueado por el proceso i
		miMutex->id_procesos_bloqueados[i] = 0;//0 si el proceso i NO ha bloqueado el mutex en la funcion "lock" 1 si el proceso i ha bloqueado el mutex en la funcion "lock"
	 }
	 numMutex++;
	 //fijar_nivel_int(nivel_interrupcion);
	 int k;
	 int resultado = -1;
	 for(i = 0; i < NUM_MUT; i++){
		if(mutex_en_sist[i].nombre!=NULL && (strcmp(mutex_en_sist[i].nombre,nombre)==0)){
			for(k = 0; k < NUM_MUT_PROC; k++){
				//if(p_proc_actual->
				if(p_proc_actual->mutex_proceso[k]==NULL){
					p_proc_actual->mutex_proceso[k]=&mutex_en_sist[i];
					resultado = k;
					p_proc_actual->nMutAsociados++;
					//nivel_interrupcion = fijar_nivel_int(NIVEL_3);
					mutex_en_sist[i].id_procesos[p_proc_actual->id]=1;
					//fijar_nivel_int(nivel_interrupcion);
					break;
				}
			}
		}
	 }
	return resultado;
}


/*Devuelve un descriptor asociado a un mutex ya existente o un numero negativo
 *en caso de error*/
int abrir_mutex(){

	char *nombre;
	nombre = (char *)leer_registro(1);

	//Comprobar si quedan descriptores disponibles
	if(p_proc_actual->nMutAsociados == NUM_MUT_PROC){
		return -1;
	}
	int i;
	int k;
	int resultado = -1;
	int nivel_interrupcion;
	for(i=0; i < NUM_MUT; i++){
		if(mutex_en_sist[i].nombre!=NULL &&(strcmp(mutex_en_sist[i].nombre,nombre)==0)){
			for(k=0;k < NUM_MUT_PROC;k++){
				if(p_proc_actual->mutex_proceso[k]==NULL){
					p_proc_actual->mutex_proceso[k]=&mutex_en_sist[i];
					resultado = k;
					p_proc_actual->nMutAsociados++;
					nivel_interrupcion = fijar_nivel_int(NIVEL_3);
					mutex_en_sist[i].id_procesos[p_proc_actual->id] = 1;
					fijar_nivel_int(nivel_interrupcion);
					break;
				}
			}
		}
	}
	return resultado;
}

int lock(){

	unsigned int mutexid;
	mutexid = (unsigned int)leer_registro(1);

	//Se intenta bloquear el proceso
	//Si el mutex est� bloqueado por otro proceso, el proceso que realiza la
	//operacion se bloquea. En caso contrario se bloquea el mutex sin bloquear
	//el proceso
	//Primero comprobar si el mutex con el identificador que me pasan existe
	//Caso en el que no exista ese mutex
	if(p_proc_actual->mutex_proceso[mutexid]==NULL){
		return -1;
	}
	//Comprobar si es de tipo NO_RECURSIVO
	if(p_proc_actual->mutex_proceso[mutexid]->tipo==NO_REC){
		if(p_proc_actual->mutex_proceso[mutexid]->bloqueos == 1){
			if(p_proc_actual->mutex_proceso[mutexid]->proc_bloq==p_proc_actual->id){
				return -2;
			}
		}
	}
	int nivel_interrupcion;

	if(p_proc_actual->mutex_proceso[mutexid]->proc_bloq!=p_proc_actual->id){
	 if(p_proc_actual->mutex_proceso[mutexid]->proc_bloq!=200){
		p_proc_actual->estado = BLOQUEADO;
		p_proc_actual->mutex_proceso[mutexid]->id_procesos_bloqueados[p_proc_actual->id]=1;
		nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		eliminar_elem(&lista_listos,p_proc_actual);
		insertar_ultimo(&lista_bloq_lock,p_proc_actual);
		fijar_nivel_int(nivel_interrupcion);

		BCP *bloqueado = p_proc_actual;
		p_proc_actual=planificador();
		cambio_contexto(&(bloqueado->contexto_regs),&(p_proc_actual->contexto_regs));
	 }
	 p_proc_actual->mutex_proceso[mutexid]->proc_bloq=p_proc_actual->id;
	 p_proc_actual->mutex_proceso[mutexid]->bloqueos = p_proc_actual->mutex_proceso[mutexid]->bloqueos+1;
	}
	return 0;
}

int unlock(){

	unsigned int mutexid;
	mutexid = (unsigned int)leer_registro(1);

	if(p_proc_actual->mutex_proceso[mutexid]==NULL){
	  return -1;
	}
	if(p_proc_actual->mutex_proceso[mutexid]->proc_bloq!=p_proc_actual->id){
	  return -2;
	}
	if(p_proc_actual->mutex_proceso[mutexid]->bloqueos==0){
	  return 0;
	}
	int nivel_interrupcion;
	//Aqui hay que diferenciar entre recursivo y no recursivo
	if(p_proc_actual->mutex_proceso[mutexid]->tipo == NO_REC){
		p_proc_actual->mutex_proceso[mutexid]->bloqueos = 0;
		p_proc_actual->mutex_proceso[mutexid]->proc_bloq = 200;

		BCP *bloqueado = lista_bloq_lock.primero;
		while(bloqueado!=NULL){
			BCP *bloqueado_siguiente = bloqueado->siguiente;
			if(p_proc_actual->mutex_proceso[mutexid]->id_procesos_bloqueados[bloqueado->id]==1){
				p_proc_actual->mutex_proceso[mutexid]->id_procesos_bloqueados[bloqueado->id]=0;
				bloqueado->estado = LISTO;
				nivel_interrupcion = fijar_nivel_int(NIVEL_3);
				eliminar_elem(&lista_bloq_lock,bloqueado);
				insertar_ultimo(&lista_listos,bloqueado);
				fijar_nivel_int(nivel_interrupcion);
			}
			bloqueado = bloqueado_siguiente;
		}
	}
	else if(p_proc_actual->mutex_proceso[mutexid]->tipo==REC){
		p_proc_actual->mutex_proceso[mutexid]->bloqueos --;
		if(p_proc_actual->mutex_proceso[mutexid]->bloqueos == 0){
			BCP *bloqueado = lista_bloq_lock.primero;
			while(bloqueado!=NULL){
			 BCP *bloqueado_siguiente = bloqueado->siguiente;
			 if(p_proc_actual->mutex_proceso[mutexid]->id_procesos_bloqueados[bloqueado->id]==1){
				p_proc_actual->mutex_proceso[mutexid]->id_procesos_bloqueados[bloqueado->id]=0;
				bloqueado->estado = LISTO;
				nivel_interrupcion = fijar_nivel_int(NIVEL_3);
				eliminar_elem(&lista_bloq_lock,bloqueado);
				insertar_ultimo(&lista_listos,bloqueado);
				fijar_nivel_int(nivel_interrupcion);
			 }
			 bloqueado = bloqueado_siguiente;
			}
			p_proc_actual->mutex_proceso[mutexid]->proc_bloq = 200;
		}
	}
	return 0;
}

int cerrar_mutex(){
	//Comprobar que el muthex con el descriptor que se pasa como parametro
	//existe y esta abierto

	unsigned int mutexid;
	mutexid = (unsigned int)leer_registro(1);

	if(p_proc_actual->mutex_proceso[mutexid]==NULL){
	  return -1;
	}
	//Comprobar si alguien lo esta utilizando
	int i;
	int nivel_interrupcion;
	int del=1;
	for(i=0;i<MAX_PROC;i++){
		if(i==p_proc_actual->id){
		 nivel_interrupcion=fijar_nivel_int(NIVEL_3);
		 p_proc_actual->mutex_proceso[mutexid]->id_procesos[i]=0;
		 fijar_nivel_int(nivel_interrupcion);
		}
		else{
		  if(p_proc_actual->mutex_proceso[mutexid]->id_procesos[i]==1){
			del = 0;
		  }
		}
	}
	if(del == 0){
		if(p_proc_actual->mutex_proceso[mutexid]->proc_bloq==p_proc_actual->id){
			p_proc_actual->mutex_proceso[mutexid]->bloqueos = 0;
			p_proc_actual->mutex_proceso[mutexid]->proc_bloq = 200;

			BCP *bloqueado = lista_bloq_lock.primero;
			while(bloqueado!=NULL){
				BCP *bloqueado_siguiente = bloqueado->siguiente;
				if(p_proc_actual->mutex_proceso[mutexid]->id_procesos_bloqueados[bloqueado->id]==1){
					p_proc_actual->mutex_proceso[mutexid]->id_procesos_bloqueados[bloqueado->id] = 0;
					bloqueado->estado=LISTO;
					nivel_interrupcion = fijar_nivel_int(NIVEL_3);
					eliminar_elem(&lista_bloq_lock,bloqueado);
					insertar_ultimo(&lista_listos,bloqueado);
					fijar_nivel_int(nivel_interrupcion);
				}
				bloqueado = bloqueado_siguiente;
			}
		}
	}
	p_proc_actual->nMutAsociados--;
	p_proc_actual->mutex_proceso[mutexid] = NULL;

	if(del == 1){
		nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		int j;
		for(j=0;j<NUM_MUT;j++){
			if((mutex_en_sist[j].nombre!=NULL)&&(p_proc_actual->mutex_proceso[mutexid]!=NULL)){
				if(strcmp(mutex_en_sist[j].nombre,p_proc_actual->mutex_proceso[mutexid]->nombre)==0){
					mutex_en_sist[j].nombre = NULL;
					break;
				}
			}
		}
		numMutex--;
		fijar_nivel_int(nivel_interrupcion);

		BCP *bloqueado = lista_bloqueados_mut.primero;
		while(bloqueado!=NULL){
			BCP *bloqueado_siguiente = bloqueado->siguiente;
			bloqueado->estado = LISTO;
			nivel_interrupcion=fijar_nivel_int(NIVEL_3);
			eliminar_elem(&lista_bloqueados_mut,bloqueado);
			insertar_ultimo(&lista_listos,bloqueado);
			fijar_nivel_int(nivel_interrupcion);
			bloqueado = bloqueado_siguiente;
		}
	}
		return 0;
}

int leer_caracter(){

	//Comprobar si hay caracteres disponibles
	//Esta funcion se encarga exclusivamente de "CONSUMIR CARACTERES"
	//Definir un buffer de caracteres
	//Un proceso llama a este servicio
	//Si hay datos disponibles se le devuelve
	//Si no se queda bloqueado esperando que haya un caracter
	//Mientras que no haya caracteres me quedo bloqueado
	int nivel_interrupcion;
	fijar_nivel_int(NIVEL_2);//Antes NIVEL_2
	while(contChar==0){
		//Me quedo bloqueado
		p_proc_actual->estado =BLOQUEADO;
	    nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		eliminar_elem(&lista_listos,p_proc_actual);
		insertar_ultimo(&lista_bloq_caracteres,p_proc_actual);
		fijar_nivel_int(nivel_interrupcion);
		//Realizamos un cambio de contexto????
		BCP *bloqueado = p_proc_actual;
		p_proc_actual = planificador();
		cambio_contexto(&(bloqueado->contexto_regs),&(p_proc_actual->contexto_regs));
	}
	//En este caso hay caracteres listos y se satisface inmediatamente
	BCP *bloqueado = lista_bloq_caracteres.primero;
	if(bloqueado!=NULL){
		bloqueado->estado = LISTO;
		nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		eliminar_elem(&lista_bloq_caracteres,bloqueado);
		insertar_ultimo(&lista_listos,bloqueado);
		fijar_nivel_int(nivel_interrupcion);
	}
	if(pcharL == TAM_BUF_TERM){
		nivel_interrupcion = fijar_nivel_int(NIVEL_2);//Antes NIVEL_2
		pcharL = 0;
		fijar_nivel_int(nivel_interrupcion);
	}
	nivel_interrupcion = fijar_nivel_int(NIVEL_2);//Antes NIVEL_2
	int caracter = caracteres[pcharL];
    pcharL++;
	contChar--;
	fijar_nivel_int(nivel_interrupcion);
	return caracter;
}

//Funcion para dormir
//Debe pasar al estado bloqueado
//Significa que el sistema pasa a estar en modo multiprogramado
//y por tanto sigue sin ser posible la existencia de llamadas al sistema concurrentes
//Sin embargo, dado que la rutina de interrupcion del reloj va a manipular listas del BCPs,
//es necesario revisar el codigo del sistema para detectar posibles problemas de sincronizacion
//en el manejo de estas listas y solventarlos elevando el nivel de interrupcion en los fragmentos
//de c�digo correspondientes.
int dormir(){
	    //El parametro 'segundos' indica el tiempo que debe bloquearse
        unsigned int segundos;
		segundos=(unsigned int)leer_registro(1);

		p_proc_actual->estado = BLOQUEADO;
		p_proc_actual->t_inicio_bloqueo = contador_actual; //contador de TICKS
		p_proc_actual->t_segundos_bloqueo = segundos;
		p_proc_actual->tics_a_dormir = TICK*segundos;
		int nivel_interrupcion = fijar_nivel_int(NIVEL_3);
		/*Eliminarlo de la lista de listos*/
		printk("Lista de listos en dormir antes de eliminar\n");
		imprimir_lista(&lista_listos);
		eliminar_elem(&lista_listos,p_proc_actual);
		printk("Lista de listos despues de eliminar\n");
		imprimir_lista(&lista_listos);
		/*Insertar al final de la lista de bloqueados al proceso actual*/
		insertar_ultimo(&lista_bloqueados,p_proc_actual);
		printk("Lista de bloqueados en dormir\n");
		imprimir_lista(&lista_bloqueados);
		//imprimir_lista(&lista_bloqueados,"bloqueados");
		//imprimir_lista(&lista_listos,"listos");
		/*Restaruamos el nivel de interrupcion anterior*/
        fijar_nivel_int(nivel_interrupcion);
		BCP *bloqueado = p_proc_actual;
		/*Llamar al planificador*/
		p_proc_actual = planificador();
		/*Realizar cambio de contexto*/
		//if(p_proc_actual!=NULL){
		cambio_contexto(&(bloqueado->contexto_regs),&(p_proc_actual->contexto_regs));
		//}else{
			//cambio_contexto(NULL,NULL);
		//}
		return 0;
}


/*
 *
 * Rutina de inicializaci�n invocada en arranque
 *
 */
int main(){
	/* se llega con las interrupciones prohibidas */

	instal_man_int(EXC_ARITM, exc_arit);
	instal_man_int(EXC_MEM, exc_mem);
	instal_man_int(INT_RELOJ, int_reloj);
	instal_man_int(INT_TERMINAL, int_terminal);
	instal_man_int(LLAM_SIS, tratar_llamsis);
	instal_man_int(INT_SW, int_sw);

	iniciar_cont_int();		/* inicia cont. interr. */
	iniciar_cont_reloj(TICK);	/* fija frecuencia del reloj */
	iniciar_cont_teclado();		/* inici cont. teclado */

	iniciar_tabla_proc();		/* inicia BCPs de tabla de procesos */

	/* crea proceso inicial */
	if (crear_tarea((void *)"init")<0)
		panico("no encontrado el proceso inicial");
	/* activa proceso inicial */
	p_proc_actual=planificador();
	cambio_contexto(NULL, &(p_proc_actual->contexto_regs));
	panico("S.O. reactivado inesperadamente");
	return 0;
}
