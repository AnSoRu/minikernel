# Minikernel project #

This repository contains a project for the Computer Science Master Degree of the Universidad
Politécnica de Madrid (MUII) https://www.fi.upm.es/?id=muii.

## Description ##

This project simulates the main functions\procedures for a very simple Linux kernel.  

It is dessirable to hava a minimum understanding of how the Linux kernel works, how the process
are choosen for execute in the processor, which one has to replace the processor when it has 
a higher priority ...

### Language ###

* C

